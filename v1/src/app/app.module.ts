import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NAV_DROPDOWN_DIRECTIVES } from './shared/nav-dropdown.directive';

import { ChartsModule } from 'ng2-charts/ng2-charts';
import { SIDEBAR_TOGGLE_DIRECTIVES } from './shared/sidebar.directive';
import { AsideToggleDirective } from './shared/aside.directive';
import { BreadcrumbsComponent } from './shared/breadcrumb.component';
import { HttpModule,Http, XHRBackend, RequestOptions } from '@angular/http';

// Routing Module
import { AppRoutingModule } from './app.routing';
import { LoginComponent } from './pages/login.component';
import { P404Component } from './pages/404.component';
import { P500Component } from './pages/500.component';
import { RegisterComponent } from './pages/register.component';
import {httpFactory} from "./http.factory";
// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';
import { HubComponent } from './hub/hub.component';
import { ActivatedRoute,Router} from "@angular/router";
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SuccessService } from './success.service';
import { FileUploaderModule } from "ng4-file-upload";
import { ExcelService } from './excel-service.service';
@NgModule({ 
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    ToastModule.forRoot(),
    BrowserAnimationsModule,
    FileUploaderModule

  ],
  declarations: [
    AppComponent,
    FullLayoutComponent,
    SimpleLayoutComponent,
    NAV_DROPDOWN_DIRECTIVES,
    BreadcrumbsComponent,
    SIDEBAR_TOGGLE_DIRECTIVES,
    AsideToggleDirective,
    LoginComponent,
    P404Component,
    P500Component,
    RegisterComponent,
    HubComponent
  ],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy,
  },{
    provide: Http,
    useFactory: httpFactory,
    deps: [XHRBackend, RequestOptions]
  },{
    provide: SuccessService,
    useClass: SuccessService
  },
  ExcelService],
  bootstrap: [ AppComponent ],
  
  
})

export class AppModule {
  constructor(private router:Router){
    this.rdirect()
  }
  rdirect(){
    if(localStorage.getItem('loginSessToken')){
      console.log("alert123")
      this.router.navigate(["/index/dashboard"])
    }else{
      console.log("alert")
      this.router.navigate(["/login"]) 
    }
  }
 }


