import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions,RequestOptionsArgs, Headers} from '@angular/http';
import { HttpModule} from '@angular/http';
import 'rxjs/add/operator/toPromise';
// @Component({
//     providers:[ApplicationStorage,getApp]
// })
@Injectable()
export class SuccessService {
 // private postUrl = 'http://localhost:3000/api/';
constructor(private http: Http) { }

//req:object
ApplicationStorage():any{
  let sessionToken = localStorage.getItem('loginSessToken');
  return sessionToken
}
parseQueryCriteria(queryCriteria):any{
console.log(queryCriteria);
var criterias = [];
if (queryCriteria.q) {
  criterias.push('q=' + encodeURI(queryCriteria.q));
}
if (queryCriteria.limit) {
  criterias.push('limit=' + queryCriteria.limit);
}
if (queryCriteria.page) {
  criterias.push('page=' + queryCriteria.page);
}
if (queryCriteria.order) {
  criterias.push('order=' + queryCriteria.order);
}
if (queryCriteria.orderBy) {
  criterias.push('orderBy=' + queryCriteria.orderBy);
}
if (queryCriteria.joinCondition) {
  criterias.push('joinCondition=' + queryCriteria.joinCondition);
}
if (queryCriteria.joinConditionList) {
  criterias.push('joinConditionList=' + queryCriteria.joinConditionList);
}
return criterias.join('&');
}

getApp(param1): any { 

 var ApiToken = this.ApplicationStorage()
 var options = new Headers();
 options.append('Content-Type', 'application/json');
 options.append('Authorization', this.ApplicationStorage())
    var url='/productDetails?' + this.parseQueryCriteria(param1)
    return this.http.get(url).toPromise()
    .then(this.extractData)
    .catch(this.handleError);   
    
  }
  getHubList(): any { 
    var ApiToken = this.ApplicationStorage()
    var options = new Headers();
    options.append('Content-Type', 'application/json');
    options.append('Authorization', this.ApplicationStorage())
       var url='/producttype'
       return this.http.get(url).toPromise()
       .then(this.extractData)
       .catch(this.handleError);   
     }
  getproductTypeList(): any { 
    var ApiToken = this.ApplicationStorage()
    var options = new Headers();
    options.append('Content-Type', 'application/json');
    options.append('Authorization', this.ApplicationStorage())
       var url='/producttype'
       return this.http.get(url).toPromise()
       .then(this.extractData)
       .catch(this.handleError);   
     }
     getTabList(): any { 

      var ApiToken = this.ApplicationStorage()
      var options = new Headers();
      options.append('Content-Type', 'application/json');
      options.append('Authorization', this.ApplicationStorage())
         var url='/tablist'
         return this.http.get(url).toPromise()
         .then(this.extractData)
         .catch(this.handleError);   
       }
      postTabList(data1): any { 
        var ApiToken = this.ApplicationStorage()
        var options = new Headers();
        options.append('Content-Type', 'application/json');
        options.append('Authorization', this.ApplicationStorage())
            var url='/tablist'
            return this.http.post(url,data1).toPromise()
            .then(this.extractData)
            .catch(this.handleError);   
        }
        updatSingleTab(data,id): any { 
        var ApiToken = this.ApplicationStorage()
        var options = new Headers();
        options.append('Content-Type', 'application/json');
        options.append('Authorization', this.ApplicationStorage())
            var url='/tablist/'+id
            return this.http.put(url,data).toPromise()
            .then(this.extractData)
            .catch(this.handleError);   
        }
        getSingleTab(id): any { 
        var ApiToken = this.ApplicationStorage()
        var options = new Headers();
        options.append('Content-Type', 'application/json');
        options.append('Authorization', this.ApplicationStorage())
            var url='/tablist/'+id
            return this.http.get(url).toPromise()
            .then(this.extractData)
            .catch(this.handleError);   
        }
        postproductList(data1): any { 
          var ApiToken = this.ApplicationStorage()
          var options = new Headers();
          options.append('Content-Type', 'application/json');
          options.append('Authorization', this.ApplicationStorage())
          var url='/productDetails'
          return this.http.post(url,data1).toPromise()
          .then(this.extractData)
          .catch(this.handleError);   
        }
        singleProductId(id): any { 
          var ApiToken = this.ApplicationStorage()
          var options = new Headers();
          options.append('Content-Type', 'application/json');
          options.append('Authorization', this.ApplicationStorage())
          var url='/productDetails/'+id
          return this.http.get(url).toPromise()
          .then(this.extractData)
          .catch(this.handleError);   
        }
        orderList(): any { 
          var ApiToken = this.ApplicationStorage()
          var options = new Headers();
          options.append('Content-Type', 'application/json');
          options.append('Authorization', this.ApplicationStorage())
          var url='/order'
          return this.http.get(url).toPromise()
          .then(this.extractData)
          .catch(this.handleError);   
        }
        getSingleProduct(data): any { 

          var ApiToken = this.ApplicationStorage()
          var options = new Headers();
          options.append('Content-Type', 'application/json');
          options.append('Authorization', this.ApplicationStorage())
             var url='/productDetails/'+data
             return this.http.get(url).toPromise()
             .then(this.extractData)
             .catch(this.handleError);   
           }
        updateSingleProduct(id,data): any { 
            var ApiToken = this.ApplicationStorage()
            var options = new Headers();
            options.append('Content-Type', 'application/json');
            options.append('Authorization', this.ApplicationStorage())
               var url='/productDetails/'+id
               return this.http.put(url,data).toPromise()
               .then(this.extractData)
               .catch(this.handleError);   
             } 
        fileUpload(file): any { 
          var ApiToken = this.ApplicationStorage()
          var options = new Headers();
          options.append('Content-Type','multipart/form-data');
          options.append('Authorization', this.ApplicationStorage());
          options.append('Accept', 'application/json');
          var url='/files/upload'
          return this.http.post(url,file).toPromise()
          .then(this.extractData)
          .catch(this.handleError);   
        }          

  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

  private handleError(error: any): Promise<any> {
      console.error('An error occurred', error);
      return Promise.reject(error.message || error);
  }
  private getRequestOptionArgs(options?: RequestOptionsArgs) : RequestOptionsArgs {
    if (options == null) {
        options = new RequestOptions();
    }
    if (options.headers == null) {
        options.headers = new Headers();
    }
    options.headers.append('Content-Type', 'application/json');

    if(localStorage.getItem('loginSessToken')){
        console.log(localStorage.getItem('loginSessToken'))
        options.headers.append('x-access-token',localStorage.getItem('loginSessToken'));
    }

    return options;
}
}
