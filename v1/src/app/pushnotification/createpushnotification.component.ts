import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { Http, Response, RequestOptions, Headers} from '@angular/http';
import { HttpModule} from '@angular/http';
import { SuccessService } from '../success.service'; 
import { TabDirective } from 'ngx-bootstrap/tabs/tab.directive';

@Component({ 
    selector: 'create-pushnotification',
    templateUrl: 'createpushnotification.component.html', 
})
export class CreatpushnotificationComponent implements OnInit {
  nData;
  constructor(
     public route:ActivatedRoute,
    private http: Http,
    private appService: SuccessService,
    //protected notificationsService: NotificationsService,
    private router:Router
  ){}
  ngOnInit() {
this.nData={}
}
}
