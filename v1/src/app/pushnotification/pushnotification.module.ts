import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// Components Routing
import {CreatpushnotificationComponent} from './createpushnotification.component';
import { PushnotificationRoutingModule } from './pushnotification-routing.module';


@NgModule({
  imports: [
    CommonModule,
    PushnotificationRoutingModule,
    FormsModule
  ],
  declarations: [
    CreatpushnotificationComponent
  ]
})
export class PushnotificationModule { }
