import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CreatpushnotificationComponent} from './createpushnotification.component';


const routes: Routes = [{
    path: '',
    component: CreatpushnotificationComponent,
    data: {
      title: 'Create Pushnotification'
    }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PushnotificationRoutingModule {}
