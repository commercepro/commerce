import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router } from '@angular/router';
import { Http, Response, RequestOptions, Headers} from '@angular/http';
import { HttpModule} from '@angular/http';
import { SuccessService } from '../success.service';

@Component({
    selector: 'customtab-list',
    templateUrl: 'customtablist.component.html',
    providers: [SuccessService]
})
export class CustomtablistComponent implements OnInit {
   public listData : any;
  constructor(public route:ActivatedRoute, private router:Router,private appService: SuccessService) {
   this.createInit();
   this.recordList(); 
}
createNew(){
   this.router.navigate(["/index/tablist/create"])
}

recordList(){
    this.appService.getTabList().then(result => {
        console.log(result.message)
        this.listData= result.message
    }) 
    .catch(error => console.log(error)); 
    
}

   createInit(){}

  ngOnInit() {
  }

}
