import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { Http, Response, RequestOptions, Headers} from '@angular/http';
import { HttpModule} from '@angular/http';
import { SuccessService } from '../success.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';


@Component({ 
    selector: 'edit-tablist',
    templateUrl: 'customtabcreate.component.html',
    providers: [SuccessService]
})
export class EditTabComponent implements OnInit {
  TabData;
  stateparam;
  constructor(
     public route:ActivatedRoute,
    private http: Http,
    private router:Router,
    private appService: SuccessService,
    public toastr: ToastsManager,
    vcr: ViewContainerRef
  ){
    
      this.toastr.setRootViewContainerRef(vcr);

  }
  submitProduct(){
  
    console.log(this.TabData)
    let dataObject = {
      tab_name:this.TabData.name,
      tab_status:this.TabData.status
    }
    this.appService.postTabList(dataObject).then(result => {
      console.log(result.message)
      this.toastr.success('Tab created success.', 'Alert!');
      this.router.navigate(["/index/tablist"])
  })
  .catch(error => console.log(error));  
  }

  gettablistSingle(){
  this.route.params.subscribe(params => { 
      console.log(params['id'])
      this.stateparam = params['id']
      this.appService.getSingleTab(this.stateparam).then(result => { 
        console.log(result);
      /*  this.productData.name = result.message.product_name
        this.productData.code = result.message.product_code
        this.productData.quantity = result.message.product_remaining_qty
        this.productData.price = result.message.product_price
        this.productData.bulk =  result.message.product_hole_sale
        this.productData.productImgUrl = result.message.product_img_url 
        this.productData.selectedValue={ name:result.message.product_type_name },
        this.productData.status = result.message.product_status*/
      })
    })
  }
  ngOnInit() {
 this.TabData={}
 this.gettablistSingle()
  }
}
