import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CustomtablistComponent} from './customtablist.component';
import {CreatTabComponent} from './customtabcreate.component';
import {EditTabComponent} from './customtabedit.component';

//import{CreathubComponent} from './createhub.component';

const routes: Routes = [
    {
    path: '',
    component: CustomtablistComponent,
    data: {
      title: 'Tab'
    }
  },{
    path: 'create',
    component: CreatTabComponent,
    data: {
      title: 'Create Tab'
    }
  },{
    path: 'edit/:id',
    component: EditTabComponent,
    data: {
      title: 'dit Tab'
    }
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class CustomtabRoutingModule {}
