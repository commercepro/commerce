import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// Components Routing
import {CustomtablistComponent} from './customtablist.component';
import { CustomtabRoutingModule } from './customtab-routing.module';
import{CreatTabComponent} from './customtabcreate.component';
import{EditTabComponent} from './customtabedit.component';

// import{CreathubComponent} from './createhub.component';


@NgModule({
  imports: [
    FormsModule, 
    CommonModule,
    CustomtabRoutingModule
  ],
  declarations: [
    CreatTabComponent,
    CustomtablistComponent,
    EditTabComponent
  ]
})
export class CustomtabModule { }
