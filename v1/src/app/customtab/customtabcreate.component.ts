import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { Http, Response, RequestOptions, Headers} from '@angular/http';
import { HttpModule} from '@angular/http';
import { SuccessService } from '../success.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';


@Component({ 
    selector: 'create-tablist',
    templateUrl: 'customtabcreate.component.html',
    providers: [SuccessService]
})
export class CreatTabComponent implements OnInit {
  TabData;
  constructor(
     public route:ActivatedRoute,
    private http: Http,
    private router:Router,
    private appService: SuccessService,
    public toastr: ToastsManager,
    vcr: ViewContainerRef
  ){
    
      this.toastr.setRootViewContainerRef(vcr);

  }
  submitProduct(){
  
    console.log(this.TabData)
    let dataObject = {
      tab_name:this.TabData.name,
      tab_status:this.TabData.status
    }
    this.appService.postTabList(dataObject).then(result => {
      console.log(result.message)
      this.toastr.success('Tab created success.', 'Alert!');
      this.router.navigate(["/index/tablist"])
  })
  .catch(error => console.log(error));  
  }
  ngOnInit() {
 this.TabData={}
  }
}
