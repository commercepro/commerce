import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router } from '@angular/router';
import { Http, Response, RequestOptions, Headers} from '@angular/http';
import { HttpModule} from '@angular/http';
import { SuccessService } from '../success.service';


@Component({
    selector: 'order-list',
    templateUrl: 'orderlist.component.html'
})

export class OrderlistComponent implements OnInit { 
   public listData : any; 
   public search:{searchText:string} = {searchText:""}
   value: any;
   private queryCriteria: Name;
  constructor(public route:ActivatedRoute, private router:Router,private appService: SuccessService) {
    this.queryCriteria={}
    this.recordList()
  }
createNew(){
 // this.router.navigate(["/index/product/create"])
  
}

recordList(){
 var Queryfilter = []
  if(this.search && this.search.searchText) { 
    Queryfilter.push({"field":"product_name","op":"contains","value":this.search.searchText})
  }
this.queryCriteria.q=JSON.stringify(Queryfilter)
this.queryCriteria.joinCondition="and"
this.queryCriteria.order="ascending"
this.queryCriteria.limit=1

this.appService.orderList().then(result => {
  console.log(result.message)
  this.listData= result.message
}) 
.catch(error => console.log(error));  
}

onEnter(){
  console.log(this.search)

}



orderBy: 'additionaldetails5'
  ngOnInit() {
  }
}
interface Name {
  q?: string;
  joinCondition?: string;
  order?:string;
  limit?:number;
}
