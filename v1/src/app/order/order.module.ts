import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// Components Routing
import { OrderlistComponent } from './orderlist.component';
import { OrderRoutingModule } from './order-routing.module';

@NgModule({
  imports: [
    CommonModule, 
    OrderRoutingModule,
    FormsModule
  ],
  declarations: [
    OrderlistComponent
  ]
})
export class OrderModule { }
