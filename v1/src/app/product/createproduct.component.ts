import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { Http, Response, RequestOptions, Headers} from '@angular/http';
import { HttpModule} from '@angular/http';
import { SuccessService } from '../success.service'; 
import { TabDirective } from 'ngx-bootstrap/tabs/tab.directive';

import * as FileSaver from 'file-saver';

@Component({ 
    selector: 'create-product',
    templateUrl: 'createproduct.component.html', 
})
export class CreatproductComponent implements OnInit {
    tabData;
  productData; 
  productImg:String;
  typeListProd:Array<{}>
  tabListProd:Array<{}>
  constructor(
     public route:ActivatedRoute,
    private http: Http,
    private appService: SuccessService,
    //protected notificationsService: NotificationsService,
    private router:Router
  ){}

  fileuploaderFileChange(files: FileList){ 
    console.log(files);
    let formData:FormData = new FormData();
        formData.append('file', files[0], files[0].name);
    this.appService.fileUpload(formData).then(result => {
console.log(result)
this.productImg = result.message.url

    })
  }
  prodTypeList(){
    this.appService.getproductTypeList().then(result => {
      this.typeListProd = result.message
      console.log(this.typeListProd)
    })
    .catch(error => console.log(error));  
     
  }
  prodTabList(){
    this.appService.getTabList().then(result => {
      this.tabListProd = result.message
      console.log(this.typeListProd)
    })
    .catch(error => console.log(error));  
     
  }

  submitProduct(){
    var postObject={
      product_name:this.productData.name,
      product_code:this.productData.code,
      product_remaining_qty:this.productData.quantity,
      product_price:this.productData.price,
      product_hole_sale:this.productData.bulk,
      product_img_url:this.productImg,
      product_tab_name:this.productData.tab_name,
      product_unit_measure:"count",
      product_description:"",
      product_festival_sale:true,
      product_type_name:this.productData.selectedValue.product_type_name,
      product_status:this.productData.status
    }
    console.log(this.productData)
    this.appService.postproductList(postObject).then(result => {
      console.log(result.message)
     // this.toastr.success('Tab created success.', 'Alert!');
      this.router.navigate(["/index/product"])
  })
  .catch(error => console.log(error));  

  }
  ngOnInit() {
 this.productData={}
 this.tabData={}
 this.prodTypeList()
 this.prodTabList()

  }
}
