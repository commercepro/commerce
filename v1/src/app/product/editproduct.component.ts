import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { Http, Response, RequestOptions, Headers} from '@angular/http';
import { HttpModule} from '@angular/http';
import { SuccessService } from '../success.service' ;


@Component({ 
    selector: 'editproduct',
    templateUrl: 'createproduct.component.html', 
})
export class EditproductComponent implements OnInit {
  public route2 :String;
  public stateparam : String;
  productData;
  constructor(
     public route:ActivatedRoute,
    private http: Http,
    private appService: SuccessService,
    //protected notificationsService: NotificationsService,
    private router:Router
  ){}



  fileuploaderFileChange(files: FileList){
    console.log(this.route);
    let formData:FormData = new FormData();
        formData.append('file', files[0], files[0].name);
    this.appService.fileUpload(formData).then(result => {
console.log(result)
    })
  }

  getDataUsingid(){ 
    console.log(this.route.params)
    this.route.params.subscribe(params => { 
      console.log(params['id'])
      this.stateparam = params['id']
      this.appService.getSingleProduct(this.stateparam).then(result => { 
        console.log(result.message);
        this.productData.name = result.message.product_name
        this.productData.code = result.message.product_code
        this.productData.quantity = result.message.product_remaining_qty
        this.productData.price = result.message.product_price
        this.productData.bulk =  result.message.product_hole_sale
        this.productData.productImgUrl = result.message.product_img_url 
        this.productData.selectedValue={ name:result.message.product_type_name },
        this.productData.status = result.message.product_status
      })
    })
  }
  
 
  submitProduct(){
    var postObject={
      product_name:this.productData.name,
      product_code:this.productData.code,
      product_remaining_qty:this.productData.quantity,
      product_price:this.productData.price,
      product_hole_sale:this.productData.bulk,
      product_img_url:"",
      product_unit_measure:"count",
      product_description:""
    }
    console.log(this.productData)
    this.appService.updateSingleProduct(this.stateparam,postObject).then(result => {
      console.log(result.message)
     // this.toastr.success('Tab created success.', 'Alert!');
      this.router.navigate(["/index/product"])
  })
  .catch(error => console.log(error));  

  }
  
  ngOnInit() { 
 this.productData={}
 this.getDataUsingid()

  }
}
