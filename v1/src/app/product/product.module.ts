import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// Components Routing
import { ProductlistComponent } from './productlist.component';
import { ProductRoutingModule } from './product-routing.module';
import{CreatproductComponent} from './createproduct.component';
import { FileUploaderModule } from "ng4-file-upload";
import {EditproductComponent} from'./editproduct.component';


@NgModule({
  imports: [
    CommonModule,
    ProductRoutingModule,
    FormsModule,
    FileUploaderModule
  ],
  declarations: [
    ProductlistComponent ,
    CreatproductComponent,
    EditproductComponent
  ]
})
export class ProductModule { }
