import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router } from '@angular/router';
import { Http, Response, RequestOptions, Headers} from '@angular/http';
import { HttpModule} from '@angular/http';
import { SuccessService } from '../success.service';
import { ExcelService } from '../excel-service.service';


@Component({
    selector: 'hub-list',
    templateUrl: 'productlist.component.html',
    providers: [SuccessService]
})

export class ProductlistComponent implements OnInit {
   public listData : any[];
   public search:{searchText:string} = {searchText:""}
   value: any;
   private queryCriteria: Name;
   persons: Person[];

  constructor(public route:ActivatedRoute, private router:Router,private appService: SuccessService,
    private excelService:ExcelService) {
    this.queryCriteria={}
    this.recordList()
  }
createNew(){
  this.router.navigate(["/index/product/create"])
  
}

recordList(){
 var Queryfilter = []
  if(this.search && this.search.searchText) { 
    Queryfilter.push({"field":"product_name","op":"contains","value":this.search.searchText})
  }
this.queryCriteria.q=JSON.stringify(Queryfilter)
this.queryCriteria.joinCondition="and"
this.queryCriteria.order="ascending"
this.queryCriteria.limit=1

this.appService.getApp(this.queryCriteria).then(result => {
  console.log(result.message)
  this.listData= result.message;
  //this.persons = PERSONS;
  //this.excelService.exportAsExcelFile(this.persons,"Sample");
}) 
.catch(error => console.log(error));  
}

onEnter(){
  console.log(this.search)

}



orderBy: 'additionaldetails5'
  ngOnInit() {
  }
}
interface Name {
  q?: string;
  joinCondition?: string;
  order?:string;
  limit?:number;
}

export class Person {
    id: number;
    name: String;
    surname: String;
    age: number;
}


export const PERSONS: Person[] = [
    {
        id: 1,
        name: 'Thomas',
        surname: 'Novicky',
        age: 21
    },
    {
        id: 2,
        name: 'Adam',
        surname: 'Tracz',
        age: 12
    },
    {
        id: 3,
        name: 'Steve',
        surname: 'Laski',
        age: 38
    }
];