import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProductlistComponent} from './productlist.component';
import{CreatproductComponent} from './createproduct.component';
import {EditproductComponent}from'./editproduct.component';


const routes: Routes = [
    {
    path: '',
    component: ProductlistComponent,
    data: {
      title: 'Product'
    }
  },{
    path: 'create',
    component: CreatproductComponent,
    data: {
      title: 'Create Product'
    }
  },{
    path: 'editproduct/:id',
    component: EditproductComponent,
    data: {
      title: 'Edit Product'
    }
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ProductRoutingModule {}
