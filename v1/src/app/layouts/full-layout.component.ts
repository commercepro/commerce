import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router} from "@angular/router";
@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html'
})
export class FullLayoutComponent implements OnInit {

  public disabled = false;
  public status: {isopen: boolean} = {isopen: false};

  public toggled(open: boolean): void {  
    console.log('Dropdown is now: ', open);
  }

  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }
  constructor( public route:ActivatedRoute, private router:Router){}
logout(){
  localStorage.removeItem('loginSessToken');
   this.router.navigate(["/login"])
  
}
  ngOnInit(): void {}
}
