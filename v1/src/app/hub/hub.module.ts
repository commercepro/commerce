import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// Components Routing
import { HublistComponent } from './hublist.component';
import { HubRoutingModule } from './hub-routing.module';
import{CreathubComponent} from './createhub.component';


@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    HubRoutingModule
  ],
  declarations: [
    CreathubComponent,
    HublistComponent 
  ]
})
export class HubModule { }
