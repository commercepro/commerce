import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router } from '@angular/router';
import { Http, Response, RequestOptions, Headers} from '@angular/http';
import { HttpModule} from '@angular/http';
import { SuccessService } from '../success.service';

@Component({
    selector: 'hub-list',
    templateUrl: 'hublist.component.html',
    providers: [SuccessService]
})
export class HublistComponent implements OnInit {
   public listData : any;
  constructor(public route:ActivatedRoute, private router:Router,private appService: SuccessService) {
   this.createInit();
   this.recordList(); 
}
createNew(){
   this.router.navigate(["/index/hub/create"])
}

recordList(){
    this.appService.getHubList().then(result => {
        console.log(result.message)
        this.listData= result.message
    })
    .catch(error => console.log(error));
    
}

   createInit(){}

  ngOnInit() {
  }

}
