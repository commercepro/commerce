import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HublistComponent} from './hublist.component';
import{CreathubComponent} from './createhub.component';


const routes: Routes = [
    {
    path: '',
    component: HublistComponent,
    data: {
      title: 'Hub'
    }
  },{
    path: 'create',
    component: CreathubComponent,
    data: {
      title: 'Create Hub'
    }
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class HubRoutingModule {}
