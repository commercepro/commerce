
import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Http, Response, RequestOptions, Headers} from '@angular/http';
import { HttpModule} from '@angular/http';
import {NgModule} from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
//import {NotificationsService} from 'angular4-notify';
import { ActivatedRoute,Router} from "@angular/router";
//import { Routes,RouterModule} from "@angular/router";

@Component({
  selector: 'toastr-component',
templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit { 
loginData;
userlist: any
loginCre = {userEmail:"chandran@gmail.com",userPassword:"demo"}
  constructor(
    public route:ActivatedRoute,
    private http: Http,
    private router:Router,
    public toastr: ToastsManager, 
    vcr: ViewContainerRef
    ) {
      this.toastr.setRootViewContainerRef(vcr);
    }
    login (){
      this.loginCre.userEmail = this.loginData.emailId
      this.loginCre.userPassword=this.loginData.password
      this.http.post('/authenticate',this.loginCre).subscribe(res1 => {
        console.log(res1)
        localStorage.setItem('loginSessToken', res1.json().token);
        localStorage.setItem('mailId', this.loginCre.userEmail); 
        this.http.get('/users'+"?userEmail="+this.loginData.emailId +"&userPassword="+ this.loginCre.userPassword).subscribe(res => {
          console.log(res)
          console.log(res.json().error)
          if(res.json().error == false){
            this.router.navigate(["/index/dashboard"])
          }else{
            this.toastr.warning('Invalid username or password.', 'Alert!');
          }
        //  this.notificationsService.addInfo('Login success');
        });
      },err => {
        this.toastr.warning('Please local start server', 'Alert!');
       // this.notificationsService.addInfo('Login error');
        console.log('Something went wrong!');
      });
    }
    

  ngOnInit() {
    this.loginData={
      emailId:"chandran@gmail.com",
      password:"demo",
      rememberMe:""
    }
  }

}
