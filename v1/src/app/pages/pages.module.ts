import { NgModule } from '@angular/core';
import { SimpleLayoutComponent } from '../layouts/simple-layout.component';

import { FullLayoutComponent } from '../layouts/full-layout.component';
import { PagesRoutingModule } from './pages-routing.module';

@NgModule({
  imports: [ PagesRoutingModule ],
  declarations: [
    SimpleLayoutComponent,
    FullLayoutComponent
  ]
})
export class PagesModule { }
